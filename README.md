## Assignment

### Goal
The goal of this assignment is to have the candidate work with the same tools that are used in every-day Chama web development, so we can learn how one would perform while solving common development tasks that we have.

### Description
For this assignment, you are supposed to build a **to-do list** with the ability to add, complete and edit some TO-DO task.

### Stack
At Chama, our frontend stack consists of [React](https://facebook.github.io/react/docs/hello-world.html) + [Redux](http://redux.js.org/) :heart:, therefore it's required that the same stack is used in this assignment. Complementary libraries are free to use (e.g. lodash).

### Firebase
Chama relies on [Firebase](https://firebase.google.com/) to give our dealers a real-time experience. Because of this, we require that some features of Firebase are used in this assignment. There is a free-plan (default) that supports the requirements of this assignment (Hosting, Auth and real-time Database) and you should be able to start creating a project with your own Google account.

### Must have
- [ ] Sign-in/Sign-out functionality using [Firebase Auth](https://firebase.google.com/docs/auth/);
- [ ] Use **[Firebase Realtime Database](https://firebase.google.com/docs/database/)** and **Redux** to keep all the TO-DO's;
- [ ] Host your working app on the *[Firebase Hosting environment](https://firebase.google.com/docs/hosting/)*;
- [ ] Assign priority to a TO-DO and sort them by **highest to lowest priority**;
- [ ] Set a due time. Add real-time visual and auditive hints to the TO-DO item that indicate that the due time is near and has passed;
- [ ] Work on Chrome.

### Nice to have
- [ ] Responsive (Mobile and Web);
- [ ] Cross-browser support;
- [ ] Tests (Unit and/or Acceptance);
- [ ] UI following [Material Design concepts](https://material.io/)

## Hints
* You don't need to spend time creating a dev/build environment, using [react-create-app](https://github.com/facebookincubator/create-react-app) (and other alike tools) is totally ok!
* Never done anything with Firebase? You can follow [this guide](https://firebase.google.com/docs/web/setup) and it should give you a nice starting point.
* If you wish to spend less time writing boilerplate code, go to http://todomvc.com/, on this website, you will find many sample implementations of the same TO-DO application, every time using a different framework. You can choose the [React](http://todomvc.com/examples/react/#/) implementation as a starting point. It's OK to use the same styling as the original application.
* Don't need to re-invent the wheel, for components like Datepickers and/or Timepicker you can use [Material-ui](https://github.com/callemall/material-ui) or any other of your choice, it has great integration with React environments.
* This is a test of your skills (mainly) with React, Redux and Firebase. Though we allow third-person libraries, keep in mind that we need to read some code of yours with those tools, so you should avoid not writing code which touches them.
* As a company, we believe that communication is the key to success. So if something is not clear to you, or if you have doubts on what you can use, reach the devs.

## Instructions
Create a new repo into your favorite git platform (github, bitbucket, etc), copy this README into it. You're free to edit it, though it should preserve the must-have functionalities and stack.

**After you finished, you can share the repository URL with us (preference) or just send us a .zip containing the source code.**

When you're done, share your repositoy's and Firebase hosting's URL.

## Review

After you delivered the completed assignment to us, we will review it as soon as we can, generally within 3 days. **We pay special attention to:**

* Coding skills
   * Writing testable code
   * Whether you use Redux, React, HTML and CSS properly
* Software Engineering Skills
   * Code organization (modularity, dependencies between modules, naming, etc)
* Overall Feeling
   * Software usability
   * Assignment completion
   * Overall code quality (edge cases, usage of tools, performance, best practices)

## Solution description

### Project structure
The project has been split into screens, containers and components. The current project has only one main screen App.
Containers - stateful components the main purpose of which is to own the state and pass data and handlers down to the presentation layer.
Components - stateles components that render data received in props.
This separation makes testing and reusing components much easier.

### Shortcuts
While implementing the assignment I made several shortcuts which didn't require extra dependencies but still demonstrate how I would approach the problem.
Notifications - I implemented "document title" notification instead of a sound notification.
Time picker - I used standard time picker instead of a custom implementation.