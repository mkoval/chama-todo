const MSEC_IN_MINUTE = 1000 * 60;

export const updateDueNotification = (items) => {
  const currentTime = new Date().getTime();

  return items.map(item => {
    if (item.due) {
      let dueInMinutes = parseInt((item.due - currentTime) / MSEC_IN_MINUTE, 10);

      return {
        ...item,
        dueInMinutes: dueInMinutes
      }
    }

    delete item.dueInMinutes;

    return item;
  })
};