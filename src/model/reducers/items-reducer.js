import {
  UPDATE_ALL_ITEMS,
  ADD_NEW_ITEM,
  REMOVE_ITEM,
  TOGGLE_ITEM,
  REMOVE_COMPLETED,
  UPDATE_PRIORITY,
  UPDATE_DUETIME,
  UPDATE_DUE_NOTIFICATION,
} from '../constants';

import { updateDueNotification } from '../utils/utils';

export default function(state = [], action) {
  if (action.type === UPDATE_ALL_ITEMS) {
    return updateDueNotification(action.items);
  }

  if (action.type === ADD_NEW_ITEM) {
    return [ { ...action.item }, ...state ];
  }

  if (action.type === REMOVE_ITEM) {
    return state.filter(item => item.id !== action.id);
  }

  if (action.type === TOGGLE_ITEM) {
    return state.map(item => {
      if (item.id === action.item.id) return { ...action.item };
      return item;
    });
  }

  if (action.type === UPDATE_PRIORITY) {
    return state.map(item => {
      if (item.id === action.item.id) return { ...action.item };
      return item;
    });
  }

  if (action.type === UPDATE_DUETIME) {
    return updateDueNotification(state.map(item => {
      if (item.id === action.item.id) return { ...action.item };
      return item;
    }));
  }

  if (action.type === REMOVE_COMPLETED) {
    return state.filter(item => item.completed === false);
  }

  if (action.type === UPDATE_DUE_NOTIFICATION) {
    return updateDueNotification(state);
  }



  return state;
}
