import {
  UPDATE_AUTH_STATE,
} from '../constants';

export default function(state = {}, action) {
  if (action.type === UPDATE_AUTH_STATE) {
    return action.user;
  }

  return state;
}
