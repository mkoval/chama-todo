import { combineReducers } from 'redux';

import items from './items-reducer';
import user from './user-reducer';

export default combineReducers({
  items,
  user
});
