import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../reducers';
import initialState from './initial-state';

import { getAllItems, startListeningToCountdown, updateDueNotification } from '../actions/items-actions';

const middleware = [thunk];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers,
  initialState,
  composeEnhancers(applyMiddleware(...middleware)),
);

store.dispatch(getAllItems());
store.dispatch(updateDueNotification());
store.dispatch(startListeningToCountdown());

export default store;
