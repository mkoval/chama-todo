import {
  UPDATE_ALL_ITEMS,
  ADD_NEW_ITEM,
  REMOVE_ITEM,
  TOGGLE_ITEM,
  REMOVE_COMPLETED,
  UPDATE_PRIORITY,
  UPDATE_DUETIME,
  UPDATE_DUE_NOTIFICATION,
} from '../constants';

import Api from '../../lib/api';

export const getAllItems = () => {
  return dispatch => {
    Api.getAll().then(items => {
      dispatch({
        type: UPDATE_ALL_ITEMS,
        items,
      });
    });
  };
};

export const addNewItem = value => {
  const item = {
    completed: false,
    priority: 2,
    value,
  };

  return dispatch => {
    Api.add(item).then(item => {
      dispatch({
        type: ADD_NEW_ITEM,
        item
      });
    });
  };
};

export const toggleItem = item => {
  const updatedItem = { ...item, completed: !item.completed };
  return (dispatch) => {
    Api.update(updatedItem).then(() => {
      dispatch({
        type: TOGGLE_ITEM,
        item: updatedItem,
      });
    });
  };
};

export const updatePriority = (item, newPriority) => {
  const updatedItem = { ...item, priority: newPriority };
  return (dispatch) => {
    Api.update(updatedItem).then(() => {
      dispatch({
        type: UPDATE_PRIORITY,
        item: updatedItem,
      });
    });
  };
};

export const updateDuetime = (item, duetime) => {
  const updatedItem = { ...item, due: duetime };
  return (dispatch) => {
    Api.update(updatedItem).then(() => {
      dispatch({
        type: UPDATE_DUETIME,
        item: updatedItem,
      });
    });
  };
};

export const removeItem = item => {
  return dispatch => {
    Api.delete(item).then(() => {
      dispatch({
        type: REMOVE_ITEM,
        id: item.id,
      });
    });
  };
};

export const removeCompleted = () => {
  return dispatch => {
    Api.deleteCompletedItems().then(() => {
      dispatch({
        type: REMOVE_COMPLETED
      });
    });
  };
};

export const updateDueNotification = () => {
  return {
    type: UPDATE_DUE_NOTIFICATION,
  }
};

export const startListeningToCountdown = () => {
  const COUNTDOWN_INTERVAL = 10 * 1000;
  return dispatch => {

    const updateTime = () => {
      dispatch(updateDueNotification());
    };

    setInterval(updateTime, COUNTDOWN_INTERVAL);
  }
};
