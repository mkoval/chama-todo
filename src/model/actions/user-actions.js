import {
  UPDATE_AUTH_STATE,
} from '../constants';

export const updateAuthState = user => {
  return {
    type: UPDATE_AUTH_STATE,
    user
  }
};