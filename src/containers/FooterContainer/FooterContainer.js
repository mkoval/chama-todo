import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Footer from '../../components/Footer/Footer';

import { removeCompleted } from '../../model/actions/items-actions';

const mapStateToProps = ({ items }) => {
  return { items: items.filter((item) => (item.completed !== true)) };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ removeCompleted }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
