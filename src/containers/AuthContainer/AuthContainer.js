import React, { Component } from 'react';
import { connect } from 'react-redux';
import firebase, { updateCurrentUser } from '../../lib/auth';

import AuthComponent from '../../components/AuthComponent/AuthComponent';
import { updateAuthState } from '../../model/actions/user-actions'

import './AuthContainer.css';

const mapStateToProps = ({ user }) => {
  return {
    user
  };
};

const mapDispatchToProps = dispatch => ({
  onAuthStateChanged(user) {
    dispatch(updateAuthState(user));
  }
});

class AuthContainer extends Component {
  componentDidMount () {
    firebase.auth().onAuthStateChanged((user) => {
      updateCurrentUser(user);
      this.props.onAuthStateChanged(user);
    });
  }

  render() {
    const { user } = this.props;

    return (
      <div className="auth-container">
        <AuthComponent user={user}/>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthContainer);
