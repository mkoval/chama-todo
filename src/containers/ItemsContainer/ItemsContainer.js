import React, { Component } from 'react';
import { connect } from 'react-redux';
import Items from '../../components/Items/Items';
import PropTypes from 'prop-types';

import {
  toggleItem,
  removeItem,
  updatePriority,
  updateDuetime,
} from '../../model/actions/items-actions'

const mapStateToProps = ({ items }) => {
  return {
    items: items.sort((a, b) => (b.priority - a.priority))
  };
};

const mapDispatchToProps = dispatch => ({
  onCheckOff(item) {
    dispatch(toggleItem(item));
  },
  onRemove(item) {
    dispatch(removeItem(item));
  },
  onPriorityUpdate(item, newPriority) {
    newPriority = parseInt(newPriority, 10);
    dispatch(updatePriority(item, newPriority))
  },
  onDuetimeUpdate(item, duetime) {
    dispatch(updateDuetime(item, duetime))
  }
});


class ItemsContainer extends Component {
  render() {
    const { items, filter, ...otherProps } = this.props,
      filteredItems = filter === 'all' ? items : items.filter((item) => {
        const isCompleted = filter === 'completed';
        return item.completed === isCompleted;
      });

    return (
      filteredItems.length !== 0 && <Items {...otherProps} items={filteredItems} />
    );
  }
}

ItemsContainer.propTypes = {
  items: PropTypes.array.isRequired,
  filter: PropTypes.string,
};

ItemsContainer.defaultProps = {
  filter: 'all',
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsContainer);
