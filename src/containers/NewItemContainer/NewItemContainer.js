import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import NewItem from '../../components/NewItem/NewItem';

import { addNewItem } from '../../model/actions/items-actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addNewItem
  }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(NewItem);
