import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Route, withRouter } from 'react-router-dom';

import NewItemContainer from '../containers/NewItemContainer/NewItemContainer';
import ItemsContainer from '../containers/ItemsContainer/ItemsContainer';
import FooterContainer from '../containers/FooterContainer/FooterContainer';
import AuthContainer from '../containers/AuthContainer/AuthContainer';

import store from '../model/store';

import {SHOW_NOTIFICATION_IF_LESS_MINUTES} from '../constants';
import './App.css';

const DOCUMENT_TITLE = document.title;

const mapStateToProps = ({ user }) => {
  return { user };
};

const mapDispatchToProps = dispatch => ({
});

class App extends Component {
  addTitleNotification = () => {
    let state = store.getState(),
      items = state.items || [],
      dueItems = 0;

    items.forEach((item) => {
      const {dueInMinutes, completed} = item;

      if (!completed && Number.isInteger(dueInMinutes) && dueInMinutes <= SHOW_NOTIFICATION_IF_LESS_MINUTES) {
        dueItems += 1;
      }
    });

    if (dueItems) {
      document.title = `(${dueItems}) Pay attention to your schedule!`;
    } else {
      document.title = DOCUMENT_TITLE;
    }
  };

  componentDidMount() {
    store.subscribe(this.addTitleNotification)
  }

  render() {
    const { user } = this.props,
      isAuthorised = user && user.isAnonymous === false;

    return (
      <div className="App">

        {!isAuthorised && (
          <AuthContainer />
        )}

        {isAuthorised && (
          <React.Fragment>
            <NewItemContainer />

            <Route exact path="/" render={()=> (
              <ItemsContainer filter="all"/>
            )}/>

            <Route path="/active" render={()=> (
              <ItemsContainer filter="active" />
            )}/>

            <Route path="/completed" render={()=> (
              <ItemsContainer filter="completed" />
            )}/>

            <FooterContainer />
          </React.Fragment>
        )}
      </div>
    );
  }
}

export default withRouter (connect(mapStateToProps, mapDispatchToProps)(App));
