export const pluralize = (noun, number) => {
  return `${noun}${number !== 1 ? 's' : '' }`;
}