import firebase from "firebase/app";
import "firebase/auth";

export const uiConfig = {
  signInSuccessUrl: '/',
  signInOptions: [
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
};

export const updateCurrentUser = (user) => {
  if (!user) {
    sessionStorage.removeItem('user');
    return;
  }
  
  let { uid, displayName } = user;
  window.sessionStorage.setItem('user', JSON.stringify({ uid, displayName }))
};

export const getCurrentUser = () => {
  let user = window.sessionStorage.getItem('user');
  if (user) {
    user = JSON.parse(user);
  }
  
  return user || {};
};

export default firebase;