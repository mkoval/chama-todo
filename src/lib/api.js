import firebase from 'firebase';
import {getCurrentUser} from './auth';
const database = firebase.database();

const getAll = async () => {
  const user = getCurrentUser();
  if (!user || !user.uid) {
    return [];
  }

  let itemsRef = await database.ref(`${user.uid}/items`).once('value'),
    items = itemsRef.val();

  return items || [];
};

const setItems = (items) => {
  const user = getCurrentUser();
  if (!user || !user.uid) {
    return false;
  }

  database.ref(`${user.uid}/items`).set(items);
};

const cleanUpItem = (item) => {
  const keys = Object.keys(item);

  keys.forEach((key) => {
    if (item[key] === undefined) {
      delete item[key];
    }
  });

  return item;
};

export default {
  async add(item) {
    const items = await getAll();
    const newItem = { ...item, id: Date.now() };
    setItems([newItem, ...items]);
    return newItem;
  },

  async getAll() {
    return getAll();
  },

  async delete({ id }) {
    const items = await getAll();
    setItems(items.filter(item => item.id !== id));
  },

  async update(updatedItem) {
    const items = await getAll();
    updatedItem = cleanUpItem(updatedItem);
    setItems(
      items.map(item => {
        if (item.id === updatedItem.id) return { ...item, ...updatedItem };
        return item;
      })
    );
  },

  async deleteCompletedItems() {
    const items = await getAll(),
      activeItems = items.filter(item => item.completed === false);
    setItems(activeItems)
  },

};
