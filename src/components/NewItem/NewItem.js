import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './NewItem.css';

class NewItem extends Component {
  state = {
    value: ''
  };

  handleChange = (event) => {
    const value = event.target.value;
    this.setState({value})
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const { value } = this.state;
    this.props.addNewItem(value);
    this.setState({value: ''});
  };

  render() {
    const { value } = this.state;

    return (
      <form className="new-item" onSubmit={this.handleSubmit}>
        <input
          className="new-item--input"
          type="text"
          placeholder="What needs to be done?"
          value={value}
          onChange={this.handleChange}
        />
      </form>
    );
  }
}

NewItem.propTypes = {
  addNewItem: PropTypes.func.isRequired,
};

export default NewItem;
