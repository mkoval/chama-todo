import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Item from '../Item/Item';
import './Items.css';

class Items extends Component {
  render() {
    const { items, onCheckOff, onRemove, onPriorityUpdate, onDuetimeUpdate } = this.props;
    return (
      <ul className="items">
        {items.map(item => (
            <Item
              {...item}

              key={item.id}
              onCheckOff={() => onCheckOff(item)}
              onRemove={() => onRemove(item)}
              onDuetimeUpdate={(duetime) => onDuetimeUpdate(item, duetime)}
              onPriorityUpdate={(priority) => onPriorityUpdate(item, priority)}
            />
          ))}
      </ul>
    );
  }
}

Items.propTypes = {
  items: PropTypes.array,
  onCheckOff: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onPriorityUpdate: PropTypes.func.isRequired,
  onDuetimeUpdate: PropTypes.func.isRequired,
};

Items.defaultProps = {
  items: [],
};

export default Items;
