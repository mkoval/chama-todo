import React, { Component } from 'react';
import firebase, {uiConfig} from '../../lib/auth';

import * as firebaseui from 'firebaseui';
import 'firebaseui/dist/firebaseui.css';
import './AuthComponent.css';
import {AUTH_CONTAINER_ID} from '../../constants';

const ui = new firebaseui.auth.AuthUI(firebase.auth());

class AuthComponent extends Component {
  componentDidMount () {
    ui.start(`#${AUTH_CONTAINER_ID}`, uiConfig);
  }

  render() {
    const { user } = this.props,
      state = user && user.state;

    return (
      <div className={`auth-component ${state ? 'auth-component__loading' : ''}`}>
          <h2 className="auth-component--please-wait">Loading... Please wait</h2>
          <div className="auth-component--please-login">
            <h2>Please login to continue</h2>
            <div id = {`${AUTH_CONTAINER_ID}`}></div>
          </div>
      </div>
    );
  }
}

export default AuthComponent;
