import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ItemsCounter from '../ItemsCounter/ItemsCounter';
import Nav from '../Nav/Nav';
import Button from '../Button/Button';
import LogoutButton from '../LogoutButton/LogoutButton';


import './Footer.css';

class Footer extends Component {
  render() {
    const {items, removeCompleted} = this.props;

    return (
      <footer className="footer">
        <div className="footer--item">
          <ItemsCounter counter={items.length} />
        </div>

        <div className="footer--item">
          <Nav />
        </div>

        <div className="footer--item">
          <Button onClick={removeCompleted} title="Clear completed" extraClass="footer--clear-completed"/>
        </div>

        <div className="footer--item">
          <LogoutButton title="Logout" extraClass="footer--logout"/>
        </div>
      </footer>
    )
  }
}


Footer.propTypes = {
  items: PropTypes.array,
};

Footer.defaultProps = {
  items: [],
};

export default Footer;
