import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './ItemsCounter.css';
import { pluralize } from '../../utils';

class ItemsCounter extends Component {
  render() {
    const {counter} = this.props;

    return (
      <span className="items-counter">{counter} {pluralize('item', counter)} left</span>
    )
  }
}


ItemsCounter.propTypes = {
  counter: PropTypes.number.isRequired,
};

export default ItemsCounter;
