import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import './Item.css';

import { SHOW_NOTIFICATION_IF_LESS_MINUTES } from '../../constants';
import { pluralize } from '../../utils';

class Item extends Component {
  onPriorityChange = (e) => {
    const priority = e.target.value;

    this.props.onPriorityUpdate(priority);
  };

  onDuetimeChange = (e) => {
    const [dueHour, dueMinute] = e.target.value && e.target.value.split(':').map(item => parseInt(item)),
      timestamp = new Date(),
      currentHour = timestamp.getHours(),
      currentMinute = timestamp.getMinutes();

    if (!Number.isInteger(dueHour) || !Number.isInteger(dueHour) ) {
      this.props.onDuetimeUpdate(undefined);
      return;
    }

    if (dueHour < currentHour || (dueHour <= currentHour && dueMinute <= currentMinute)) {
      timestamp.setDate(timestamp.getDate() + 1)
    }

    timestamp.setHours(dueHour);
    timestamp.setMinutes(dueMinute);

    this.props.onDuetimeUpdate(timestamp.getTime());
  };

  getDuetime24h(timestamp) {
    let dueTime = new Date(timestamp),
      dueMinutes = dueTime.getMinutes(),
      dueHours = dueTime.getHours();

    dueMinutes = dueMinutes < 10 ? `0${dueMinutes}` : dueMinutes;
    dueHours = dueHours < 10 ? `0${dueHours}` : dueHours;

    return `${dueHours}:${dueMinutes}`;
  }

  render() {
    const { completed, id, value, priority, onCheckOff, onRemove, dueInMinutes, due } = this.props,
      isOverdue = !Math.max(dueInMinutes, 0);

    let notificationMessage = '',
      dueTime24h = '';

    if (due) {
      dueTime24h = this.getDuetime24h(due)
    }

    if (dueInMinutes <= SHOW_NOTIFICATION_IF_LESS_MINUTES) {
      notificationMessage = isOverdue ? 'overdue!' : `due in ${dueInMinutes} ${pluralize('minute', dueInMinutes)}`
    }

    return (
      <li className={`item ${(isOverdue && !completed)? 'item__overdue' : ''}`} >
        <input
          className="item--toggle"
          type="checkbox"
          checked={completed}
          onChange={onCheckOff}
          id={id}
        />

        <div className={`item-priority item-priority__priority-${priority}`}></div>

        <label htmlFor={id}>
          {value}
        </label>

        {notificationMessage && !completed &&
          <span className={`item--due ${isOverdue ? 'item--due__overdue' : ''}`}>{notificationMessage}</span>
        }

        <div className="item--tools">
          <div className="item--tool-item item--tool-item-due">
            <span className="item--due-label">Due time: </span><input value={dueTime24h} onChange={this.onDuetimeChange} disabled={completed} className="item--due-time" type="time" />
          </div>

          <div className="item--tool-item item--tool-item-priority">
            <select disabled={completed} className="item--priority" name="priority" value={priority} onChange={this.onPriorityChange}>
              <option value="3">High</option>
              <option value="2">Normal</option>
              <option value="1">Low</option>
            </select>
          </div>

          <div className="item--tool-item">
            <Button onClick={onRemove} title="Remove" extraClass="item-btn item--remove" />
          </div>
        </div>

        
      </li>
    );
  }
}

Item.propTypes = {
  completed: PropTypes.bool,
  id: PropTypes.number.isRequired,
  value: PropTypes.string.isRequired,
  priority: PropTypes.number.isRequired,
  due: PropTypes.number,
  dueInMinutes: PropTypes.number,
  onCheckOff: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onPriorityUpdate: PropTypes.func.isRequired,
  onDuetimeUpdate: PropTypes.func.isRequired,
};

export default Item;
