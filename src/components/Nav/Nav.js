import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './Nav.css';

class Nav extends Component {
  render() {
    return (
        <div className="app-nav">
          <Link to="/">All</Link>
          <Link to="/active">Active</Link>
          <Link to="/completed">Completed</Link>
        </div>
    )
  }
}

export default Nav;
