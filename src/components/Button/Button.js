import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Button.css';

class Button extends Component {
  onClick = (e) => {
    this.props.onClick(e);  
  };
  
  render() {
    const { title, extraClass } = this.props;
    return (
      <button className={`button ${extraClass}`} onClick={this.onClick}>{title}</button>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string,
  extraClass: PropTypes.string
};

Button.defaultProps = {
  title: 'Button'
};

export default Button;
