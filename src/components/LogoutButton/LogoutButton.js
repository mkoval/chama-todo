import React, { Component } from 'react';
import Button from '../Button/Button';

import firebase from '../../lib/auth';

import './LogoutButton.css';

class LogoutButton extends Component {
  onClick = ()=> {
    firebase.auth().signOut();
  };
  
  render() {
    return (
      <Button {...this.props} onClick={this.onClick} extraClass={`${this.props.extraClass} login-button`}/>
    );
  }
}


export default LogoutButton;
