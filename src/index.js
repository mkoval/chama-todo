import React from 'react';
import ReactDOM from 'react-dom';

import './lib/app';

import {BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './model/store';

import App from './screens/App';

import './index.css';

ReactDOM.render(
  <BrowserRouter><Provider store={store}><App /></Provider></BrowserRouter>, 
  document.getElementById('root')
);

